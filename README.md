# Phong lighting
Phong lighting is Java Swing application presenting Phong reflection model

## Functionalities implemented
* Presenting 3 objects (spheres) on the scene
* Visualisation of light reflection for 3 different materials using Phong model

## Technologies used
* Java
* Swing library
* Maven
* MVP pattern