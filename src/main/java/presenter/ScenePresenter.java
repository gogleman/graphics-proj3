package presenter;

import lombok.Getter;
import lombok.Setter;
import model.Scene;
import model.Sphere;
import view.MainFrame;
import view.MaterialButtons;

/**
 * Zarządza wyświatlaniem obiektów na scenie
 */
public class ScenePresenter {

    /* todo Dodać pobieranie informacji o natężeniu światła w punktach 3D oraz przekazywanie punktów 2D do widoku w celu wyświetlenia na ekranie
     */
    @Getter
    private Scene scene;
    @Getter @Setter
    private MainFrame mainFrame;

    public ScenePresenter() {
        scene = new Scene();
    }

    public ScenePresenter(Scene scene) {
        this.scene = scene;
    }

    public void closeButtonClicked(){
        System.exit(0);
    }

    public void materialButtonClicked(MaterialButtons.ButtonType buttonType){
        switch (buttonType){
            case FIRST_MATERIAL:{
                //System.out.println("First material pressed");
                makeSphereVisible(0);
                break;
            }
            case SECOND_MATERIAL:{
                //System.out.println("Second material pressed");
                makeSphereVisible(1);
                break;
            }
            case THIRD_MATERIAL:{
                //System.out.println("Third material pressed");
                makeSphereVisible(2);
                break;
            }
        }

        mainFrame.repaint();
    }

    private void makeSphereVisible(int id){
        for (Sphere sphere : scene.getSpheres()){
            if(sphere.getId() == id ) {
                sphere.setVisible(true);
            }
            else sphere.setVisible(false);
        }
    }

}
