package core;

/**
 * Punkt 3D
 */
public class Point3D extends Point2D {

    private double z;

    public Point3D(double x, double y, double z) {
        super(x, y);
        this.z = z;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Point3D point3D = (Point3D) o;

        return z == point3D.z;
    }

    @Override
    public int hashCode() {
        double result = super.hashCode();
        result = 31 * result + z;
        return (int)result;
    }

    @Override
    public String toString() {
        return "Point3D{" +
                "x=" + getX() +
                " y=" + getY() +
                " z=" + z +
                '}';
    }
}
