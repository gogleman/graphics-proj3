import core.Point3D;
import model.Properties;
import model.Scene;
import model.Sphere;
import presenter.ScenePresenter;
import view.MainFrame;

public class Main {

    public static void main (String [] args){

        Properties properties1 = new Properties(0.25,0.75,30,new Point3D(400,400,-3000),150,0,0);
        Sphere sphere1 = new Sphere(0,0,0,0,50,properties1);
        sphere1.setVisible(true);
        //Properties properties2 = new Properties(0.25,0.75,10,new Point3D(400,400,-400),0,150,0);
        //Sphere sphere2 = new Sphere(1,0,0,0,50,properties2);
        //Properties properties3 = new Properties(0.25,0.75,10,new Point3D(400,400,-400),0,0,150);
        //Sphere sphere3 = new Sphere(2,0,0,0,50,properties3);

        Scene scene = new Scene();
        scene.addSphere(sphere1);
        //scene.addSphere(sphere2);
        //scene.addSphere(sphere3);

        ScenePresenter scenePresenter = new ScenePresenter(scene);
        MainFrame mainFrame = new MainFrame(scenePresenter);
        scenePresenter.setMainFrame(mainFrame);

        mainFrame.open();
    }
}
