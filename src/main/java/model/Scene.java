package model;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * Grupuje obiekty na scenie
 */
public class Scene {
    @Getter
    private List<Sphere> spheres;

    public Scene() {
        spheres = new ArrayList<Sphere>();
    }

    public Scene(List<Sphere> spheres) {
        this.spheres = spheres;
    }

    public void addSphere(Sphere sphere){
        spheres.add(sphere);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for(Sphere sphere : spheres){
            stringBuilder.append(sphere);
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }
}
