package model;

import core.Point2D;

import java.awt.*;

/**
 * Punkt 2D wraz z kolorem
 */
public class ColorPoint2D extends Point2D {

    private Color color;
    private static final Color DEFAULT_COLOR = Color.LIGHT_GRAY;


    public ColorPoint2D(int x, int y, Color color) {
        super(x, y);
        this.color = color;
    }

    public ColorPoint2D(double x, double y) {
        super(x, y);
        this.color = DEFAULT_COLOR;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ColorPoint2D that = (ColorPoint2D) o;

        return color != null ? color.equals(that.color) : that.color == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (color != null ? color.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ColorPoint2D{" +
                "x=" + this.getX() +
                "y=" + this.getY() +
                "color=" + color +
                '}';
    }
}
