package model;

import core.Point3D;

/**
 * Punkt 3D kuli, zawiera informację o natężeniu światła
 */
public class SpherePoint3D extends Point3D {

    private double lighting;
    private static final int DEFAULT_LIGHTING = 0;

    public SpherePoint3D(int x, int y, int z, int lighting) {
        super(x, y, z);
        this.lighting = lighting;
    }


    public SpherePoint3D(double x, double y, double z) {
        super(x, y, z);
        this.lighting = DEFAULT_LIGHTING;
    }

    public double getLighting() {
        return lighting;
    }

    public void setLighting(double lighting) {
        this.lighting = lighting;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        SpherePoint3D that = (SpherePoint3D) o;

        return lighting == that.lighting;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int)lighting;
        return result;
    }

    @Override
    public String toString() {
        return "SpherePoint3D{" +
                "x=" + this.getX() +
                "y=" + this.getY() +
                "z=" + this.getZ() +
                "lighting=" + lighting +
                '}';
    }
}
