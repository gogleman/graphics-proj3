package model;

import algorithms.DrawingAlgorithms;
import algorithms.LightAlgorithms;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Kula
 */

public class Sphere {

    private int coordX;
    private int coordY;
    private int coordZ;
    @Getter
    private int id;
    @Getter
    private int radius;
    @Getter
    private Properties properties;
    @Getter
    private List<SpherePoint3D> points3d;
    @Getter @Setter
    private List<ColorPoint2D> points2d;
    @Getter @Setter
    private boolean isVisible = false;

    public Sphere(int id, int x, int y, int z, int radius, Properties properties) {
        this.id = id;
        this.coordX = x;
        this.coordY = y;
        this.coordZ = z;
        this.radius = radius;
        this.properties = properties;

        points3d = new ArrayList<>();
        points2d = new ArrayList<>();
        calculatePoints3d();
        calculatePoints2d();
    }

    private void calculatePoints3d(){
        double step = 0.1;
        double lightStrength;

        for(double x = -radius; x<radius; x+=step){
            for(double y = -radius; y<radius; y+=step){
                for(double z = -radius; z<=0; z+=step){
                    double pointDistance = (x * x) + (y * y) + (z * z);

                    if (Math.abs(pointDistance - (radius * radius)) <= 5) {
                        SpherePoint3D newPoint = new SpherePoint3D(x, y, z);
                        lightStrength = LightAlgorithms.calculateLightStrength(newPoint,properties);
                        newPoint.setLighting(lightStrength);

                        points3d.add(newPoint);
                    }
                }
            }
        }
    }


    private void calculatePoints2d(){
        /*for(SpherePoint3D point3D : points3d){
            points2d.add(DrawingAlgorithms.transform3dTo2d(point3D,100));
            points2d.get(points2d.size()-1).setColor(DrawingAlgorithms.calculateColor(this,properties));
        }
        */
        this.setPoints2d(DrawingAlgorithms.calculateColors(this,properties));

    }

    @Override
    public String toString(){
        return "x: " + coordX + ", y: " + coordY + ", z: " + coordZ + ", r: " + radius;
    }

}
