package model;


import core.Point3D;
import lombok.Getter;

public class Properties {

    @Getter
    private double ks;
    @Getter
    private double kd;
    @Getter
    private int n;
    @Getter
    private Point3D lightPoint;
    @Getter
    private int colorR;
    @Getter
    private int colorG;
    @Getter
    private int colorB;

    private final int DEFAULT_LIGHTPOINT_X = 300;
    private final int DEFAULT_LIGHTPOINT_Y = 300;
    private final int DEFAULT_LIGHTPOINT_Z = -300;

    public Properties(double ks, double kd, int n, int x, int y, int z, int colorR, int colorG, int colorB) {
        this.ks = ks;
        this.kd = kd;
        this.n = n;
        this.lightPoint = new Point3D(x,y,z);
        this.colorR = colorR;
        this.colorG = colorG;
        this.colorB = colorB;
    }

    public Properties(double ks, double kd, int n) {
        this.ks = ks;
        this.kd = kd;
        this.n = n;
        this.lightPoint = new Point3D(DEFAULT_LIGHTPOINT_X,DEFAULT_LIGHTPOINT_Y,DEFAULT_LIGHTPOINT_Z);
    }

    public Properties(double ks, double kd, int n, Point3D lightPoint, int colorR, int colorG, int colorB) {
        this.ks = ks;
        this.kd = kd;
        this.n = n;
        this.lightPoint = lightPoint;
        this.colorR = colorR;
        this.colorG = colorG;
        this.colorB = colorB;
    }
}
