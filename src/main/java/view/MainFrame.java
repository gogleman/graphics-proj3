package view;

import lombok.Setter;
import model.Scene;
import presenter.ScenePresenter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EnumMap;
import java.util.Map;

/**
 * Główne okno aplikacji
 */
public class MainFrame extends JFrame implements ActionListener{

    @Setter
    private ScenePresenter scenePresenter;
    private ScenePanel scenePanel;
    private Container container;
    private JPanel buttonsPanel;
    private JButton close;
    private MaterialButtons materialButtons;
    private EnumMap<MaterialButtons.ButtonType, JButton> materialButtonsMap;


    public final static int FRAME_WIDTH = 800;
    public final static int FRAME_HEIGHT = 500;

    public MainFrame(){
        super("Project 3");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(FRAME_WIDTH,FRAME_HEIGHT);
        prepareGUI();
    }

    public MainFrame(ScenePresenter scenePresenter){
        this();
        this.scenePresenter = scenePresenter;
        setSceneOnPanel(scenePresenter.getScene());

    }

    private void prepareGUI(){
        scenePanel = new ScenePanel();
        buttonsPanel = new JPanel();
        close = new JButton("Close");

        scenePanel.setBackground(Color.BLUE);
        buttonsPanel.setBackground(Color.RED);

        MaterialButtons materialButtons = new MaterialButtons();
        this.materialButtonsMap = materialButtons.getMaterialButtonsMap();

        //System.out.println("Pierwszy materiał: " + materialButtonsMap.get(MaterialButtons.ButtonType.FIRST_MATERIAL));

        for(Map.Entry<MaterialButtons.ButtonType,JButton> entry : materialButtonsMap.entrySet()){
            buttonsPanel.add(entry.getValue());
            entry.getValue().addActionListener(this);
        }

        buttonsPanel.add(close);
        close.addActionListener(this);

        container = this.getContentPane();
        container.add(scenePanel);
        container.add(buttonsPanel,BorderLayout.AFTER_LAST_LINE);
        this.setFocusable(true);
    }

    public void open(){
        this.setVisible(true);
    }

    public ScenePanel getScenePanel() {
        return scenePanel;
    }

    private void setSceneOnPanel(Scene scene){
        this.scenePanel.setScene(scene);
    }


    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();

        if(source == close){
            scenePresenter.closeButtonClicked();
        }
        else {
            for(Map.Entry entry : materialButtonsMap.entrySet()){
                if(entry.getValue() == source){
                    scenePresenter.materialButtonClicked((MaterialButtons.ButtonType)entry.getKey());
                }
            }
        }
    }
}
