package view;

import javax.swing.*;
import java.util.EnumMap;

/**
 * Przyciski
 */
public class MaterialButtons {

    public enum ButtonType{
        FIRST_MATERIAL,
        SECOND_MATERIAL,
        THIRD_MATERIAL,
        FOURTH_MATERIAL,
    }

    private JButton first_material;
    private JButton second_material;
    private JButton third_material;
    private JButton fourth_material;

    private static EnumMap<ButtonType,JButton> materialButtonsMap;

    public MaterialButtons(){
        createButtons();
        addButtonsToMap();
    }

    private void createButtons(){
        first_material = new JButton("First material");
        second_material = new JButton("Second material");
        third_material = new JButton("Third material");
        fourth_material = new JButton("Fourth material");
    }

    private void addButtonsToMap(){
        materialButtonsMap = new EnumMap<ButtonType, JButton>(ButtonType.class);

        materialButtonsMap.put(ButtonType.FIRST_MATERIAL,first_material);
        materialButtonsMap.put(ButtonType.SECOND_MATERIAL,second_material);
        materialButtonsMap.put(ButtonType.THIRD_MATERIAL,third_material);
        //materialButtonsMap.put(ButtonType.FOURTH_MATERIAL,fourth_material);
    }

    public EnumMap<ButtonType, JButton> getMaterialButtonsMap() {
        return materialButtonsMap;
    }
}

