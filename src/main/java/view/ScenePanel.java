package view;

import lombok.Setter;
import model.ColorPoint2D;
import model.Scene;
import model.Sphere;

import javax.swing.*;
import java.awt.*;

import static view.MainFrame.FRAME_HEIGHT;
import static view.MainFrame.FRAME_WIDTH;

/**
 * Scena z kulą
 */
public class ScenePanel extends JPanel{

    @Setter
    private Scene scene;


    public ScenePanel() {

    }

    public void paintComponent(Graphics g){
        g.setColor(Color.RED);
        drawSphere(g);
    }

    private void drawSphere(Graphics g){
        //g.drawString("Loading image...",FRAME_WIDTH/2,FRAME_HEIGHT/2);
        for(Sphere sphere : scene.getSpheres()){
            if(sphere.isVisible()){
                for(ColorPoint2D point2D : sphere.getPoints2d()) {
                    int x = (int) (point2D.getX()) + FRAME_WIDTH / 2;
                    int y = (int) (-(point2D.getY() - FRAME_HEIGHT / 2));
                    //System.out.println("point2D.getX(): " + point2D.getX() + ", point2D.getY(): " + point2D.getY());
                    g.setColor(point2D.getColor());
                    //System.out.println(point2D.getColor());
                    //g.setColor(new Color(sphere.getProperties().getColorR(),sphere.getProperties().getColorG(),sphere.getProperties().getColorB()));
                    g.drawLine(x, y, x, y);
                }
            }
        }
        System.out.println("Drawing finished");
    }
}
