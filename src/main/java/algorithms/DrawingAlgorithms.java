package algorithms;

import model.ColorPoint2D;
import model.Properties;
import model.Sphere;
import model.SpherePoint3D;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Zawiera metody do wyliczania koloru punktu 2D na podstawie natężenia w punkcie 3D
 * Zawiera metody do mapowania pozycji 3D na 2D
 */
public class DrawingAlgorithms {

    public static ColorPoint2D transform3dTo2d(SpherePoint3D spherePoint3D, int distance){

        double x = (spherePoint3D.getX() * distance) / (spherePoint3D.getZ()+distance);
        double y = (spherePoint3D.getY() * distance) / (spherePoint3D.getZ()+distance);

        return new ColorPoint2D(x,y);
    }

    //todo Dodać metodę wyliczania koloru punktu 2D

    public static Color calculateColor(Sphere sphere, Properties properties){

        int colorR = properties.getColorR();
        int colorG = properties.getColorG();
        int colorB = properties.getColorB();


        return new Color(colorR,colorG,colorB);
    }

    public static java.util.List<ColorPoint2D> calculateColors(Sphere sphere, Properties properties){

        int colorR = properties.getColorR();
        int colorG = properties.getColorG();
        int colorB = properties.getColorB();
        int maxReflection = 10;
        int minReflection = 0;

        java.util.List<SpherePoint3D> reflections = new ArrayList<>();
        java.util.List<ColorPoint2D> colorPoints2D = new ArrayList<>();


        reflections = sphere.getPoints3d();
        SpherePoint3D reflectionMin = Collections.min(reflections, (p1, p2) -> (int) (p1.getLighting() - p2.getLighting()));

        double reflectionRangeStart = -reflectionMin.getLighting();
        //System.out.println("reflectionRangeStart: " + reflectionRangeStart);

        double range = maxReflection - minReflection;

        int maxColor =0 ;
        int minColor = 255;

        for (SpherePoint3D reflection : reflections) {
            int color = (int)((reflection.getLighting() + reflectionRangeStart) / (range / 255));

            if( color < 0 ){
                color = 0;
            }
            if( color > 255 ){
                color = 255;
            }


            if(color>maxColor) maxColor = color;
            if(color<minColor) minColor = color;

            Color sphereColor = new Color(
                    Math.max(scale(color, 255, colorR, 255, 0),0),
                    Math.max(scale(color, 255, colorG, 255, 0),0),
                    Math.max(scale(color, 255, colorB, 255, 0),0));

            colorPoints2D.add(transform3dTo2d(reflection, 100));
            colorPoints2D.get(colorPoints2D.size()-1).setColor(sphereColor);

            //System.out.println("Color: " + color + " | Sphere color: " + sphereColor);
        }

        System.out.println("MinColor: " + minColor + " | MaxColor: " + maxColor);

        return colorPoints2D;


    }


    private static int scale(double x,double max,double min,double largest,double smallest){
        return (int)(min + (max-min)/(largest-smallest)*(x-smallest));
    }

}


