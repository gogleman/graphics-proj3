package algorithms;

import core.Point3D;
import model.Properties;
import model.SpherePoint3D;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import static org.apache.commons.math3.geometry.euclidean.threed.Vector3D.crossProduct;
import static org.apache.commons.math3.geometry.euclidean.threed.Vector3D.dotProduct;

/**
 * Zawiera metody pozwalające wyliczyć natężenie światła w danym miejscu
 */

/*todo Dodać funkcje wyliczające natężenie światła w danym punkcie na podstawie odległości od źródła, rodzaju materiału*/

public class LightAlgorithms {

    public static double calculateLightStrength(SpherePoint3D spherePoint3D, Properties properties){

        double lightStrength;
        Point3D lightPoint = properties.getLightPoint();

        Vector3D vectorN = new Vector3D(spherePoint3D.getX(),spherePoint3D.getY(),spherePoint3D.getZ());
        //System.out.println("nVec: " + vectorN);

        Vector3D vectorL = new Vector3D(lightPoint.getX()-spherePoint3D.getX(),lightPoint.getY()-spherePoint3D.getY(),lightPoint.getZ()-spherePoint3D.getZ());
        //System.out.println("lVec: " + vectorL);
        double angle = Math.acos(dotProduct(vectorN.normalize(), vectorL.normalize()));
        //System.out.println("angle: " + angle);
        Vector3D cross = crossProduct(vectorN, vectorL);
        double dot = dotProduct(vectorL, cross);
        if (dot < 0) {
            angle = -angle;
        }

        System.out.println("Cross: " + cross + " | dot: " + dot);

        lightStrength = 0.01*properties.getKd() * dot + 60 * properties.getKs() * Math.pow(Math.cos(angle),properties.getN());
       // System.out.println(lightStrength);
        return lightStrength;

    }
}
